<?php

/**
 * Initialization of the packages
 */
require_once __DIR__ .'/../vendor/autoload.php';

use Dotenv\Dotenv;
use PHPMailer\PHPMailer\PHPMailer;

// init .env properties
Dotenv::createImmutable(__DIR__ . '/..')->load();


// getting properties from .env
$username = getenv('MAIL_USERNAME');
$password = getenv('MAIL_PASSWORD');


$mail = new PHPMailer(true);
$mail->CharSet = 'UTF-8';
$email = $_POST['email-hire'];
$name = $_POST['name-hire'];
$cover = $_POST['cover'];
$resume = $_POST['resume'];
//var_dump($_FILES);
//die();

//smtp details
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'TLS';
$mail->Port = '587';

//credentials
$mail->Username = $username;
$mail->Password = $password;

$mail->setFrom($email,$name);
//recipients
$mail->addAddress('people.culture@3rdilab.com');
//$mail->addAddress('anna.logacheva@3rdilab.com');
if (isset($_FILES['resume_file']) && $_FILES['resume_file']['error'] === 0) {
$mail->AddAttachment($_FILES['resume_file']['tmp_name'], $_FILES['resume_file']['name']);
}

$mail->isHTML(true);
$mail->Subject = '3rdi Lab Job Form';
$mail->Body    = 'Name: ' .$name .'<br>Cover Letter: ' .$cover.'<br>Email: ' .$email.'<br>resume: ' .$resume;
$mail->AltBody = '';

if(!$mail->send()) {
    echo 'Mailer error: ' . $mail->ErrorInfo;
} else {
    echo 'Success!';
}
