/*global require*/

const {
  STYLE_GUID_FOLDER,
  STYLE_WATCH_PATH,
  STYLE_SRC_PATH,
  JS_WATCH_PATH,
  HTML_WATCH_PATH,
  HTML_INCLUDES_WATCH_PATH,
  HTML_COMPONENTS_WATCH_PATH,
  WORK_OUT_FOLDER,
  PROD_FOLDER
} = require('./config.json');

//Gulp related dependencies
const gulp      = require('gulp'),
  gulpif        = require('gulp-if'),
  sass          = require('gulp-sass'),
  babel         = require('gulp-babel'),
  rename        = require('gulp-rename'),
  concat        = require('gulp-concat'),
  uglify        = require('gulp-uglify'),
  useref        = require('gulp-useref'),
  plumber       = require('gulp-plumber'),
  imagemin      = require('gulp-imagemin'),
  size          = require('gulp-filesize'),
  gulpStylelint = require('gulp-stylelint'),
  minifyCSS     = require('gulp-minify-css'),
  autoprefixer  = require('gulp-autoprefixer'),
  gulpWatch     = require('gulp-watch'),
  browsersync   = require('browser-sync').create(),
  newer         = require("gulp-newer"),
  shell         = require('gulp-shell'),
  fileinclude = require('gulp-file-include');

gulp.task('styleguid', shell.task('styledown public/styleguid/config.md > public/styleguid/styleguide.html'))


function fileInclude() {
  return gulp.src([WORK_OUT_FOLDER + '/includes/' + '*.html'])
      .pipe(fileinclude({
        prefix: '@@',
        basepath: WORK_OUT_FOLDER
      }))
      .pipe(gulp.dest(WORK_OUT_FOLDER));
}

function browserSync(done) {
  browsersync.init({
    open:false,
    injectChanges:true,
    server: [WORK_OUT_FOLDER,STYLE_GUID_FOLDER]
  })
  done();
}
function browserSyncReload(done) {
  browsersync.reload();
  done();
}

function js() {
  return gulp.src([WORK_OUT_FOLDER + 'js/main.js'])
      .pipe(plumber())
}

function vendorsCss() {
  return gulp.src([
    './node_modules/@fortawesome/fontawesome-free/css/all.css',
    './node_modules/bootstrap/dist/css/bootstrap.css',
    './node_modules/animate.css/animate.css',
    './node_modules/slick-carousel/slick/slick.css',
    './node_modules/lightgallery/dist/css/lightgallery.min.css',
  ])
      .pipe(concat('vendors.css'))
      .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'));
}

function vendorsJs() {
  return gulp.src([
    './node_modules/jquery/dist/jquery.js',
    './node_modules/@fortawesome/fontawesome-free/js/all.js',
    './node_modules/bootstrap/dist/js/bootstrap.min.js',
    './node_modules/wowjs/dist/wow.js',
    './node_modules/slick-carousel/slick/slick.min.js',
    './node_modules/gsap/dist/gsap.js',
    './node_modules/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
    './node_modules/gsap/dist/ScrollToPlugin.js',
    './node_modules/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js',
    './node_modules/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js',
    './node_modules/lightgallery/dist/js/lightgallery.min.js',
  ])
      .pipe(babel())
      .pipe(concat('vendors.js'))
      .pipe(gulp.dest(WORK_OUT_FOLDER + 'js'));
}

// Optimize Images
function images() {
  return gulp.src(WORK_OUT_FOLDER + 'images/**/*')
      .pipe(newer(PROD_FOLDER + 'images'))
      .pipe(imagemin({verbose: true}))
      .pipe(gulp.dest(PROD_FOLDER + 'images'));
}

function favicons(){
  return gulp.src([WORK_OUT_FOLDER + '*.png',WORK_OUT_FOLDER + '*.ico'])
      .pipe(gulp.dest(PROD_FOLDER));
}

function videos(){
  return gulp.src(WORK_OUT_FOLDER + 'videos/**/*.*')
      .pipe(gulp.dest(PROD_FOLDER + 'videos'));
}

function php(){
  return gulp.src(WORK_OUT_FOLDER + './**/*.php')
      .pipe(gulp.dest(PROD_FOLDER))
}

function fonts(){
  return gulp.src('./node_modules/@fortawesome/fontawesome-free/webfonts/*')
      .pipe(gulp.dest(WORK_OUT_FOLDER + 'webfonts'))
}

function fontsToDist(){
  gulp.src([WORK_OUT_FOLDER + 'webfonts/*.*'])
      .pipe(gulp.dest(PROD_FOLDER + 'webfonts'))

  return gulp.src(WORK_OUT_FOLDER + 'fonts/*.*')
      .pipe(gulp.dest(PROD_FOLDER + 'fonts'))
}

function lintCss() {
  return gulp.src('src/**/*.scss')
      .pipe(gulpStylelint({
        reporters: [
          { formatter: 'string', console: true }
        ],
        debug: true
      }));
}

function scssToCss(){
  return gulp.src(WORK_OUT_FOLDER + 'scss/style.scss')
      .pipe(sass())
      .pipe(autoprefixer({
        browsers: ['last 10 versions'],
        cascade: false
      }))
      .pipe(gulp.dest(WORK_OUT_FOLDER + 'css'))
      .pipe(browsersync.stream());
}

function watchFiles(done) {
  gulp.watch( STYLE_WATCH_PATH, gulp.series([lintCss,scssToCss]));
  gulp.watch( JS_WATCH_PATH, gulp.series([js, browserSyncReload]));
  gulp.watch( HTML_WATCH_PATH, gulp.series([browserSyncReload]));
  gulp.watch( HTML_INCLUDES_WATCH_PATH, gulp.series([fileInclude,browserSyncReload]));
  gulp.watch( HTML_COMPONENTS_WATCH_PATH, gulp.series([fileInclude,browserSyncReload]));
//   // gulp.watch(STYLE_GUID_FOLDER + '**/*.md', ['styleguid']);
//   // gulp.watch(STYLE_GUID_FOLDER + '**/*.css', ['styleguid']);
//   // gulp.watch(STYLE_GUID_FOLDER + '*.html',browserSyncReload);
  done();
}

function compileBundle() {
  return gulp.src([WORK_OUT_FOLDER + '*.html'])
      .pipe(useref())
      .pipe(gulpif('main.js', uglify()))
      .pipe(gulpif('*.css', minifyCSS()))
      .pipe(gulp.dest(PROD_FOLDER))
      .pipe(size());
}

const css = gulp.series(lintCss, scssToCss);
const vendors = gulp.series(vendorsCss, vendorsJs);
const build = gulp.series(gulp.parallel(css,js,vendors,fileInclude));
const watch = gulp.parallel(watchFiles, browserSync);
const bundle = gulp.series([build,images,favicons,videos,fontsToDist,php,compileBundle]);


// export tasks
exports.images = images;
exports.favicons = favicons;
exports.videos = videos;
exports.php = php;
exports.fonts = fonts;
exports.fontsToDist = fontsToDist;
exports.fileInclude = fileInclude;
exports.css = css;
exports.js = js;
exports.vendors = vendors;
exports.build = build;
exports.watch = watch;
exports.default = gulp.series([build,watch]);
exports.bundle = bundle;
