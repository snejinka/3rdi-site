<?php

/**
 * Initialization of the packages
 */
require_once __DIR__ .'/../vendor/autoload.php';

use Dotenv\Dotenv;
use PHPMailer\PHPMailer\PHPMailer;

// init .env properties
Dotenv::createImmutable(__DIR__ . '/..')->load();


// getting properties from .env
$username = getenv('MAIL_USERNAME');
$password = getenv('MAIL_PASSWORD');


$mail = new PHPMailer(true);
$mail->CharSet = 'UTF-8';
$email = $_POST['email'];
$name = $_POST['name'];
$category = $_POST['category'];
$project_name = $_POST['project_name'];
$issue = $_POST['issue'];
//var_dump($_FILES);
//die();

//smtp details
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'TLS';
$mail->Port = '587';

//credentials
$mail->Username = $username;
$mail->Password = $password;

$mail->setFrom($email,$name);
//recipients
$mail->addAddress('contact.us@3rdilab.com');
$mail->addAddress('anna.logacheva@3rdilab.com');
if (isset($_FILES['issue_file']) && $_FILES['issue_file']['error'] === 0) {
$mail->AddAttachment($_FILES['issue_file']['tmp_name'], $_FILES['issue_file']['name']);
}

$mail->isHTML(true);
$mail->Subject = '3rdi Lab Report Issue Form';
$mail->Body    = 'Name: ' .$name .'<br>Category: ' .$category.'<br>Project name: ' .$project_name.'<br>Email: ' .$email.'<br>Issue: ' .$issue;
$mail->AltBody = '';

if(!$mail->send()) {
    echo 'Mailer error: ' . $mail->ErrorInfo;
} else {
    echo 'Success!';
}
