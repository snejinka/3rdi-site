<?php

/**
 * Initialization of the packages
 */
require_once __DIR__ .'/../vendor/autoload.php';

use Dotenv\Dotenv;
use PHPMailer\PHPMailer\PHPMailer;

// init .env properties
Dotenv::createImmutable(__DIR__ . '/..')->load();


// getting properties from .env
$username = getenv('MAIL_USERNAME');
$password = getenv('MAIL_PASSWORD');


$mail = new PHPMailer(true);
$mail->CharSet = 'UTF-8';
$email = $_POST['email'];
$name = $_POST['name'];
$feature = $_POST['feature'];
$follow = $_POST['follow'];
$update = $_POST['update'];

//smtp details
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'TLS';
$mail->Port = '587';

//credentials
$mail->Username = $username;
$mail->Password = $password;

$mail->setFrom($email,$name);
//recipients
$mail->addAddress('contact.us@3rdilab.com');

$mail->isHTML(true);
$mail->Subject = '3rdi Lab Report Issue Form';
$mail->Body    = 'Name: ' .$name .'<br>Email: ' .$email.'<br>In the future, I’d love to see in 3rdi: ' .$feature.'<br>Can we follow up in your experience?: ' .$follow.'<br>Can we send you periodic Email updates?: ' .$update;
$mail->AltBody = '';

if(!$mail->send()) {
    echo 'Mailer error: ' . $mail->ErrorInfo;
} else {
    echo 'Success!';
}
