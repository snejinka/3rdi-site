<?php

/**
 * Initialization of the packages
 */
require_once __DIR__ .'/../vendor/autoload.php';

use Dotenv\Dotenv;
use PHPMailer\PHPMailer\PHPMailer;

// init .env properties
Dotenv::createImmutable(__DIR__ . '/..')->load();


// getting properties from .env
$username = getenv('MAIL_USERNAME');
$password = getenv('MAIL_PASSWORD');


$mail = new PHPMailer(true);
$mail->CharSet = 'UTF-8';
$email = $_POST['email-feedback'];
$name = $_POST['name-feedback'];
$message = $_POST['feedback-message'];
$file = $_POST['feedback-file'];
//var_dump($_FILES);
//die();

//smtp details
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'TLS';
$mail->Port = '587';

//credentials
$mail->Username = $username;
$mail->Password = $password;

$mail->setFrom($email,$name);
//recipients
$mail->addAddress('user.feedback@3rdilab.com');
//$mail->addAddress('anna.logacheva@3rdilab.com');
if (isset($_FILES['feedback_file']) && $_FILES['feedback_file']['error'] === 0) {
$mail->AddAttachment($_FILES['feedback_file']['tmp_name'], $_FILES['feedback_file']['name']);
}

$mail->isHTML(true);
$mail->Subject = '3rdi Lab Feedback Form';
$mail->Body    = 'Name: ' .$name .'<br>Message: ' .$message.'<br>Email: ' .$email.'<br>File: ' .$file;
$mail->AltBody = '';

if(!$mail->send()) {
    echo 'Mailer error: ' . $mail->ErrorInfo;
} else {
    echo 'Success!';
}
