<?php

/**
 * Initialization of the packages
 */
require_once __DIR__ .'/../vendor/autoload.php';

use Dotenv\Dotenv;
use PHPMailer\PHPMailer\PHPMailer;

// init .env properties
Dotenv::createImmutable(__DIR__ . '/..')->load();


// getting properties from .env
$username = getenv('MAIL_USERNAME');
$password = getenv('MAIL_PASSWORD');


$mail = new PHPMailer(true);
$mail->CharSet = 'UTF-8';
$name = $_POST['name'];
$lastname = $_POST['lastname'];
$company_name = $_POST['company_name'];
$email = $_POST['email'];
$country = $_POST['country'];

//smtp details
$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'TLS';
$mail->Port = '587';

//credentials
$mail->Username = $username;
$mail->Password = $password;

$mail->setFrom($email,$name);
//recipients
$mail->addAddress('contact.us@3rdilab.com');

$mail->isHTML(true);
$mail->Subject = '3rdi Lab Contact Form';
$mail->Body    = 'Name: ' .$name .'<br>Last name: ' .$lastname.'<br>Company name: ' .$company_name.'<br>Email: ' .$email.'<br>Country: ' .$country;
$mail->AltBody = '';

if(!$mail->send()) {
    echo 'Mailer error: ' . $mail->ErrorInfo;
} else {
    echo 'Success!';
}
