//fix google map issue
function initMap() {}

$(function () {

    let team = [
        {
            "id": 1,
            "name": "Raj Nayak",
            "photo_fullsize_src": "./images/team/raj-fullsize.png",
            "marker_src": "./images/team/markers/raj-marker.png",
            "about": "Fueled by passion and driven by innovation, Keymaker, as Raj is rightfully known, is the soothsayer whose vision opens doors for people to see the world with their third eye.",
            "lat": 45.08670810092575,
            "lng": -64.36245932547698,
            "country": 'Canada',
            "city": 'Wolfville',
            "role": 'CEO',
            "email": 'raj.nayak@3rdilab.com',
        },
        // {
        //     "id": 2,
        //     "name": "Abhishek Kamath",
        //     "photo_fullsize_src": "./images/team/abhishek-fullsize.png",
        //     "about": "Rightly bestowed with the title of Code Maven, Abhishek is the techie whose impeccable skills help us create ground-breaking mobile application experiences.",
        //     "lat": 34.23217499864038,
        //     "lng": -65.71477904037353,
        //     // "country": 'Canada',
        //     // "city": 'Hallifax',
        //     "role": 'Software Development',
        //     "email": 'abhishek.kamath@3rdilab.com',
        // },
        {
            "id": 3,
            "name": "Sierra Francis",
            "photo_fullsize_src": "./images/team/sierra-fullsize.png",
            "marker_src": "./images/team/markers/sierra-marker.png",
            "about": "When she’s not busy playing an instrument in a band, Sierra transforms into the Neural Neophyte who composes immaculate software symphonies and makes the Python sing like a choir.",
            "lat": 52.32076967384568,
            "lng": -102.08230731701151,
            "country": 'Canada',
            "city": 'Saskatoon',
            "role": 'AI Engineer',
            "email": 'sierra.francis@3rdilab.com',
        },
        {
            "id": 4,
            "name": "Yi Wang",
            "photo_fullsize_src": "./images/team/yi-fullsize.png",
            "marker_src": "./images/team/markers/yi-marker.png",
            "about": "Responsible for breaking the ground with his remarkable machine learning prowess, Yi, the Neural Ninja, can make you see things and experience objects in a way that others can’t.",
            "lat": 59.70939531013856,
            "lng": -103.74077919510529,
            "country": 'Canada',
            "city": 'Saskatoon',
            "role": 'Machine Learning / Ai',
            "email": 'yi.wang@3rdilab.com',
        },
        {
            "id": 5,
            "name": "Ramesh Nayak",
            "photo_fullsize_src": "./images/team/ramesh-fullsize.png",
            "marker_src": "./images/team/markers/ramesh-marker.png",
            "about": "With supernormal power comes phenomenal responsibility and that’s what Ramesh is destined to do. Known for his dedication and valuable presence, the Tech Jedi leads the system design and development process.",
            "lat": 12.908817439252912,
            "lng": 74.85535099939216,
            "country": 'India',
            "city": 'Mangalore',
            "role": 'Software Development',
            "email": 'ramesh.nayak@3rdilab.com',
        },
        {
            "id": 6,
            "name": "Anna Logacheva",
            "photo_fullsize_src": "./images/team/anna-fullsize.png",
            "marker_src": "./images/team/markers/anna-marker.png",
            "about": "Polishing and packaging everything to make it postcard perfect, Anna – the Beauty Maker – is responsible for handling all of our front end and design-related intricacies.",
            "lat": 55.0240111974793,
            "lng": 73.37988349643116,
            "country": 'Russia',
            "city": 'Omsk',
            "role": 'UI/UX Designer and Frontend Developer',
            "email": 'anna.logacheva@3rdilab.com',
        },
        // {
        //     "id": 7,
        //     "name": "Vlad Radchenko",
        //     "photo_fullsize_src": "./images/team/vlad-fullsize.png",
        //     "about": "Working behind the scenes to ensure that the magic never runs out, Vlad the Code Maven is the genius whose technical prowess always keeps us ahead of the game.",
        //     "lat": 48.52155306150658,
        //     "lng": 34.573350935114675,
        //     "country": 'Ukraine',
        //     "city": 'Dniprodzerzhynsk',
        //     "role": 'Back End Developer',
        //     "email": 'vlad.radchenko@3rdilab.com',
        // },
        // {
        //     "id": 8,
        //     "name": "Ananya Sharma",
        //     "photo_fullsize_src": "./images/team/vlad-fullsize.png",
        //     "about": "Working behind the scenes to ensure that the magic never runs out, Vlad the Code Maven is the genius whose technical prowess always keeps us ahead of the game.",
        //     "lat": 28.37924779573345,
        //     "lng": -65.01165406235265,
        //     "country": 'India',
        //     "city": 'Bangalore',
        //     "role": 'AI Intern',
        //     "email": 'ananya.sharma@3rdilab.com',
        // },
        {
            "id": 9,
            "name": "Hannah Eliason",
            "photo_fullsize_src": "./images/team/hannah-fullsize.png",
            "marker_src": "./images/team/markers/hannah-marker.png",
            "about": "Working behind the scenes to ensure that the magic never runs out, Vlad the Code Maven is the genius whose technical prowess always keeps us ahead of the game.",
            "lat": 49.67831621557598,
            "lng": -112.81686796556602,
            "country": 'Canada',
            "city": 'Lethbridge',
            "role": 'VR Content Creator',
            "email": 'hannah.eliason@3rdilab.com',
        },
        {
            "id": 10,
            "name": "Divya Shenoy",
            "photo_fullsize_src": "./images/team/divya-fullsize.png",
            "marker_src": "./images/team/markers/divya-marker.png",
            "about": "Working behind the scenes to ensure that the magic never runs out, Vlad the Code Maven is the genius whose technical prowess always keeps us ahead of the game.",
            "lat": 51.269023988928595,
            "lng": 9.968062925949852,
            "country": 'Germany',
            "city": 'Hannover',
            "role": 'Testing',
            "email": 'divya.shenoy@3rdilab.com',
        },
        {
            "id": 11,
            "name": "Lujie Duan",
            "photo_fullsize_src": "./images/team/lujie-fullsize.png",
            "marker_src": "./images/team/markers/lujie-marker.png",
            "about": "Working behind the scenes to ensure that the magic never runs out, Vlad the Code Maven is the genius whose technical prowess always keeps us ahead of the game.",
            "lat": 57.2140980389695,
            "lng": -109.95958926326458,
            "country": 'Canada',
            "city": 'Saskatoon',
            "role": 'AI App Development',
            "email": 'lujie.duan@3rdilab.com',
        }
    ];

    $('.anchor').on('click', function(e){
        e.preventDefault();
        var elem = $(this).attr('href'),
            positionscroll = $(elem).offset().top;
        $('body,html').animate({scrollTop:positionscroll}, 1000);

    });

    new WOW().init();

    //forms
    let contactForm = $('#form-contact');
    let contactFormModal = $('#form-contact-modal');
    let issueForm = $('#form-issue');
    let hireForm = $('#form-hire');
    let feedbackForm = $('#form-feedback');

    function submitForm(form) {
        let formMessages = form.children('.form-messages');
        $(form).submit(function(event) {
            event.preventDefault();
            $(form).find('.spinner').css('display', 'inline-block');
            $(form).find('.btn-spinner').prop('disabled', true);

            var formData = $(form).serialize();
            $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: new FormData(this),
                processData: false,
                contentType: false
            }).done(function (data) {
                $(formMessages).removeClass('error');
                $(formMessages).addClass('success');
                $(formMessages).text(data);
                $(formMessages).delay(5000).fadeOut();
                $(form).find('.form-control').val('');
                $(form).find('.form-group > .btn-spinner > .spinner').delay(300000).css('display', 'none');
                $(form).find('.btn-spinner').prop('disabled', false);
            }).fail(function(err) {
                $(formMessages).removeClass('success');
                $(formMessages).addClass('error');
                if (err.statusText !== '') {
                    $(formMessages).text(err.statusText);
                } else {
                    $(formMessages).text('Oops! An error occured and your message could not be sent.');
                }
                $(formMessages).delay(5000).fadeOut();
                $(form).find('.form-group > .btn-spinner > .spinner').delay(300000).css('display', 'none');
                $(form).find('.btn-spinner').prop('disabled', false);
            });
        })
    }

    submitForm(contactForm)
    submitForm(contactFormModal)
    submitForm(issueForm)
    submitForm(hireForm)
    submitForm(feedbackForm)

    //slick slider
    var breakpoint = {
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1200
    };

// page slider
    let paramsSlider = {
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: true,
        autoplaySpeed: 2000,
        draggable: true,
        infinite: true,
        arrows: true,
        speed: 1000,
        mobileFirst: false,
        prevArrow:"<button type='button' class='slick-arrow slick-arrow_dark slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
        nextArrow:"<button type='button' class='slick-arrow slick-arrow_dark slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
        responsive: [
            {
                breakpoint: breakpoint.sm,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: breakpoint.md,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: breakpoint.lg,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: breakpoint.xl,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }
        ]
    };
    $('#team-slider').slick(paramsSlider);
    $('#advisors-slider').slick(paramsSlider);

    let paramsTutorialsSlider = {
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        draggable: true,
        infinite: true,
        arrows: true,
        speed: 1000,
        mobileFirst: false,
        prevArrow:"<button type='button' class='slick-arrow slick-arrow_dark slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
        nextArrow:"<button type='button' class='slick-arrow slick-arrow_dark slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
        responsive: [
            {
                breakpoint: breakpoint.sm,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: breakpoint.md,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: breakpoint.lg,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: breakpoint.xl,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }
        ]
    };
    $('#tutorials-slider').slick(paramsTutorialsSlider);

    let paramsGetStartedSlider = {
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        draggable: true,
        infinite: false,
        arrows: true,
        speed: 1000,
        mobileFirst: false,
        prevArrow:"<button type='button' class='slick-arrow slick-arrow_dark slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
        nextArrow:"<button type='button' class='slick-arrow slick-arrow_dark slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>",
    };
    $('#get-started-slider').slick(paramsGetStartedSlider);

    function makeCardsTheSameHeight() {
        var cards = $('#team-slider .teammate-card');
        cards.height('auto')
        var maxHeight = 0;

        // Loop all cards and check height
        for (let i = 0; i < cards.length; i++) {
            if (maxHeight < $(cards[i]).outerHeight()) {
                maxHeight = $(cards[i]).outerHeight();
            }
        }
        //Set bigger height to all cards
        for (let i = 0; i < cards.length; i++) {
            $(cards[i]).height(maxHeight);
        }
    }

    makeCardsTheSameHeight();

    window.addEventListener("resize", makeCardsTheSameHeight);

//Click on contact button
    $('.contact-btn').on('click', () => {
        $('#contactModal').modal('show')
    })

    $("#issue_file").on('change', (e) => {
        let files = e.target.files;
        var names = $.map(files, (val) => { return val.name; });
        if(names){
            names.map((name) => {
                $('.file-list').append(`<span>${name}</span>`)
            })
        }
    })

    $("#resume_file").on('change', (e) => {
        let files = e.target.files;
        var names = $.map(files, (val) => { return val.name; });
        if(names){
            names.map((name) => {
                $('.file-list').append(`<span>${name}</span>`)
            })
        }
    })
    $("#feedback_file").on('change', (e) => {
        let files = e.target.files;
        var names = $.map(files, (val) => { return val.name; });
        if(names){
            names.map((name) => {
                $('.file-list').append(`<span>${name}</span>`)
            })
        }
    })

    //fix header
    $(window).on('scroll', function() {
        if ($(this).scrollTop() >= 200) {
            $('.header-navbar').addClass('fixed-top');
        } else if ($(this).scrollTop() === 0) {
            $('.header-navbar').removeClass('fixed-top');
        }
        let phoneSectionHeight = $('#main-section-with-phone').length ? $('#main-section-with-phone')[0].offsetHeight : $('#full-height-wrap')[0].offsetHeight;
        let phoneVideo = document.getElementById("phone-video");
        if(phoneVideo){
            if(!phoneVideo.classList.contains('playing')) return;
            if($(this).scrollTop() > (phoneSectionHeight /2)){
                phoneVideo.pause();
            }
            if($(this).scrollTop() <= (phoneSectionHeight /2)){
                phoneVideo.play();
            }
        }

    });
    //menu animation
    let controller = new ScrollMagic.Controller({
        globalSceneOptions: {
            duration: $('.full-height-wrap').height(),
            triggerHook: .025,
            reverse: true
        }
    });

    let scene1 = $("#benefits-section").length && new ScrollMagic.Scene({triggerElement: '#benefits-section'})
        .setClassToggle('#benefits-anchor', 'active')
        .addTo(controller);
    let scene2 = $("#partnership-section").length && new ScrollMagic.Scene({triggerElement: '#partnership-section'})
        .setClassToggle('#partnership-anchor', 'active')
        .addTo(controller);
    let scene3 = $("#advantage-section").length && new ScrollMagic.Scene({triggerElement: '#advantage-section'})
        .setClassToggle('#advantage-anchor', 'active')
        .addTo(controller);
    let scene4 = $("#about-section").length && new ScrollMagic.Scene({triggerElement: '#about-section'})
        .setClassToggle('#about-anchor', 'active')
        .addTo(controller);
    let scene5 = $("#experience-section").length && new ScrollMagic.Scene({triggerElement: '#experience-section'})
        .setClassToggle('#experience-anchor', 'active')
        .addTo(controller);
    controller.scrollTo(function(target) {

        TweenMax.to(window, 0.5, {
            scrollTo : {
                y : target,
                autoKill : true // Allow scroll position to change outside itself
            },
            ease : Cubic.easeInOut
        });

    });

    $(document).on("click", "a[href^='#']", function (e) {
        let id = $(this).attr("href");
        if ($(id).length > 0) {
            e.preventDefault();

            // trigger scroll
            controller.scrollTo(id);
            if (window.history && window.history.pushState) {
                history.pushState("", document.title, id);
            }
        }
    });

//animate depends on video time
    let phoneVideo = document.getElementById("phone-video");
    if(phoneVideo) phoneVideo.ontimeupdate = function() {myFunction()};

    function myFunction() {
        let {currentTime} = phoneVideo;
        let section = $('.phone-section-wrap');
        let rectangles = $('#rectangles');
        let colWithPhone = $('#col-with-phone');
        // console.log(currentTime)
        if(currentTime === 0 || currentTime < 5){
            section.removeClass('phase-rotated phase-sculpture phase-show-vr phase-increase-headset phase-show-headset phase-vr-end phase-show-vr-text phase-back-to-landscape')
            rectangles.removeClass('phase-go-down phase-rotated phase-go-right')
            colWithPhone.removeClass('col-md-6 col-12 order-md-last order-first mb-md-0 py-md-0 py-5').addClass('col-12')
            if(phoneVideo.classList.contains('stopped') && phoneVideo.classList.contains('playing')){
                phoneVideo.classList.remove('playing','stopped');
                phoneVideo.pause()
                $('.phone-section-wrap').removeClass('phase-started')
                $('#unmute-popup').show();
            }
        }
        if(currentTime < 11 || (currentTime > 15 && currentTime < 27.5)) return;
        if(currentTime >= 11 && currentTime <= 15){
            rectangles.addClass('phase-go-down')
        }else if(currentTime >= 27.5 && currentTime < 37.5){
            section.addClass('phase-rotated')
            rectangles.addClass('phase-rotated')
            colWithPhone.removeClass('col-12').addClass('col-md-6 col-12 order-md-last order-first mb-md-0 py-md-0 py-5')
        }
        else if(currentTime >= 37.5 && currentTime <= 47){
            section.addClass('phase-sculpture')
            rectangles.addClass('phase-go-right')
        }
        else if(currentTime >= 47 && currentTime < 79){
            section.addClass('phase-back-to-landscape')
            section.removeClass('phase-rotated phase-sculpture')
            colWithPhone.removeClass('col-md-6 col-12 order-md-last order-first mb-md-0 py-md-0 py-5').addClass('col-12')
        }
        else if(currentTime >= 79 && currentTime < 80){
            section.addClass('phase-show-vr-text')
        }
        else if(currentTime >= 80 && currentTime < 84){
            section.addClass('phase-show-headset')
        }
        else if(currentTime >= 84 && currentTime < 85){
            section.addClass('phase-increase-headset')
        }
        else if(currentTime >= 85 && currentTime < 196){
            section.addClass('phase-show-vr')
        }
        else if(currentTime >= 196){
            section.removeClass('phase-increase-headset')
            section.addClass('phase-vr-end')
            phoneVideo.classList.add('stopped');
        }
    }

    $('#swap-btn').on('click', () => {
        phoneVideo.currentTime += 10;
    })
    $('#pause-btn').on('click', () => {
        $('#pause-btn').hide();
        phoneVideo.pause()
        $('.phone-section-wrap').removeClass('phase-started')
        $('#unmute-popup').show();
        phoneVideo.classList.remove('playing');
    })
    $('#play-btn').on('click', () => {
        phoneVideo.play()
    })

    $('#btn-unmute').on('click', () => {
        $('.covid-alert').addClass('covid-alert__collapse')
        phoneVideo.play()
        phoneVideo.classList.add('playing');
        $('#unmute-popup').hide();
        $('.phone-section-wrap').addClass('phase-started')
    })

    let videoArea = $(".phone-section");
    if(videoArea){
        videoArea.mouseleave(() => {
            if($(".phone-section-wrap").hasClass('phase-started')){
                $('#pause-btn').hide();
            }
        })

        videoArea.mouseenter(() => {
            if($(".phone-section-wrap").hasClass('phase-started')){
                $('#pause-btn').show();
            }
        })
    }


    $('.covid-alert').on('click', () => {
        if($('.covid-alert').hasClass('covid-alert__collapse')){
            $('.covid-alert__collapse').removeClass('covid-alert__collapse')
        }
    })

    $('#vision-tab').on('click', () => {
        $('.about-section').addClass('vision-content');
    })
    $('.about-nav .nav-link:not(#vision-tab)').on('click', () => {
        $('.about-section').removeClass('vision-content');
    })
    $('#office-tab').on('click', () => {
        $('.about-section').addClass('office-content');
    })
    $('.about-nav .nav-link:not(#office-tab)').on('click', () => {
        $('.about-section').removeClass('office-content');
    })
    $('#mission-tab').on('click', () => {
        $('.about-section').addClass('mission-content');
    })
    $('.about-nav .nav-link:not(#mission-tab)').on('click', () => {
        $('.about-section').removeClass('mission-content');
    })

    $('.header-navbar .nav-link').on('click', (e) => {
        e.stopPropagation();
        $('.header-navbar .navbar-collapse').collapse('hide');
        $(".navbar-toggler").removeClass("active");
    })


    //team map
    let mapEl = document.getElementById("team-map");
    initMap = function() {
        if(!mapEl) return;
        const center = { lat: 31.633652823447648, lng: -21.595244154941685 };
        const map = new google.maps.Map(mapEl, {
            zoom: 1,
            center: center,
            minZoom: 1,
            maxZoom: 1,
            scrollwheel: false,
            disableDoubleClickZoom: true,
            disableDefaultUI: true,
            restriction: {
                latLngBounds: {north: 84.92280987080538, south: -38.90726323391822, west: -180, east: 180},
                strictBounds: true
            },
            mapId:"1575c789c1bbdf81",
        });

        const infowindow = new google.maps.InfoWindow();

        for (let i = 0; i < team.length; i++) {
            const teammate = team[i];
            const markerIcon = {
                url: teammate.marker_src, // url
                scaledSize: new google.maps.Size(50, 50), // scaled size
                // origin: new google.maps.Point(0,0), // origin
                // anchor: new google.maps.Point(0, 0) // anchor
            };
            let marker = new google.maps.Marker({
                position: { lat: teammate.lat, lng: teammate.lng },
                map,
                icon: markerIcon,
                title: teammate.name,
                zIndex: teammate.id,
            })
            const content = '  <div class="team-card team-popup">' +
                '        <!--        todo update photos when we have new ones-->' +
                '        <div class="team-popup__photo-wrap">' +
                `        <img src="${teammate.photo_fullsize_src}" alt="" class="team-popup__photo">` +
                '        </div>' +
                `        <div class="team-card__name team-popup__name">${teammate.name}</div>` +
                `        <div class="team-card__role">${teammate.role}</div>` +
                `        <a class="text-dark team-card__email team-popup__email" target="_blank" href = "mailto:${teammate.email}">${teammate.email}</a>` +
                `        <div class="team-popup__location">${teammate.city}, ${teammate.country}</div>` +
                '    </div>'

            google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){
                return function() {
                    infowindow.setContent(content);
                    infowindow.setOptions({maxWidth:320});
                    infowindow.open(map,marker);
                };
            })(marker,content,infowindow));
        }
    }
    initMap()


    //hamburger
    let hamburgerMenu = $(".navbar-toggler");
    hamburgerMenu.click( function(e) {
        $(this).toggleClass("active");
    });

    $(window).click(function() {
        if($('.header-navbar .navbar-collapse').hasClass('show')){
            hamburgerMenu.removeClass("active");
            $('.header-navbar .navbar-collapse').collapse('hide');
        }
    });
    let mediaQueryList = window.matchMedia("(max-width: 768px)");

    function addClassToAlert() {
        if (mediaQueryList.matches) {
            $('.covid-alert').addClass('covid-alert__collapse')
        }
    }
    addClassToAlert()

    mediaQueryList.addListener(addClassToAlert);

    $(".covid-alert__close").on("click", (e) => {
        e.stopPropagation();
        $('.covid-alert').addClass('covid-alert__collapse')
    })

    //light gallery
    let gallery = $("#light-gallery");

    gallery.lightGallery({
        download:false,
        selector: '.gallery__item-lg',
        subHtmlSelectorRelative: true
    });

    $("#enter-gallery").on("click", () => {
        $("#light-gallery .gallery__item:first-of-type .gallery__item-lg").trigger("click");
    });

    $("#show-instructions").on("click", () => {
        $('#gallery__modal').modal('show');
    });

    $('#gallery__modal').modal('show');
});
